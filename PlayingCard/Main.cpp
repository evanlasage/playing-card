#include <iostream>
#include <conio.h>
#include <string>
using namespace std;

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
	Clubs = 1, Hearts, Diamonds, Spades
};

struct Card
{
	Rank rank;
	Suit suit;
};

void PrintCard(Card card)
{
	std::cout << "The ";

	switch (card.rank)
	{
	case Two: cout << "Two "; break;
	case Three: cout << "Three "; break;
	case Four: cout << "Four "; break;
	case Five: cout << "Five "; break;
	case Six: cout << "Six "; break;
	case Seven: cout << "Seven "; break;
	case Eight: cout << "Eight "; break;
	case Nine: cout << "Nine "; break;
	case Ten: cout << "Ten "; break;
	case Jack: cout << "Jack "; break;
	case Queen: cout << "Queen "; break;
	case King: cout << "King "; break;
	case Ace: cout << "Ace "; break;
	}

	std::cout << "of ";

	switch (card.suit)
	{
	case Clubs: cout << "Clubs"; break;
	case Hearts: cout << "Hearts"; break;
	case Diamonds: cout << "Diamonds"; break;
	case Spades: cout << "Spades"; break;
	}

	std::cout << "\n";
}



Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		return card1;
	}
	else
	{
		return card2;
	}
}


int main()
{
	Card c1;
	Card c2;

	c1.rank = Two;
	c1.suit = Clubs;

	c2.rank = Six;
	c2.suit = Hearts;

	PrintCard(c1);

	PrintCard(c2);

	HighCard(c1, c2);





	_getch();
	return 0;
}